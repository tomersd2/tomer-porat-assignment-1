import re
from bs4 import BeautifulSoup


def test_if_string_1_found(get_response):
    soup = BeautifulSoup(get_response.text, 'html.parser')
    string_to_find = "Right-click in the box below to see one called 'the-internet'"
    is_string_found = bool(soup.find_all(string=re.compile(string_to_find)))
    assert is_string_found is True


def test_if_string_2_found(get_response):
    soup = BeautifulSoup(get_response.text, 'html.parser')
    string_to_find = "Alibaba"
    is_string_found = bool(soup.find_all(string=re.compile(string_to_find)))
    assert is_string_found is False
