import responses
import requests
from bs4 import BeautifulSoup
import re


@responses.activate
def test_string_1_mocked():
    responses.add(responses.GET, 'https://the-internet.herokuapp.com/context_menu', body="Right-click in the box below to see one called 'the-internet'")
    response = requests.get('https://the-internet.herokuapp.com/context_menu')
    soup = BeautifulSoup(response.text, 'html.parser')
    string_to_find = "Right-click in the box below to see one called 'the-internet'"
    is_string_found = bool(soup.find_all(string=re.compile(string_to_find)))
    assert is_string_found is True
