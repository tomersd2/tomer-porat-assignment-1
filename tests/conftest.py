import pytest
import requests


@pytest.fixture
def get_response():
    url = "https://the-internet.herokuapp.com/context_menu"
    try:
        response = requests.get(url)
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        print(err)

    else:
        return response
